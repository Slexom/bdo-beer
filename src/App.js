import React from 'react';
import styles from "./style.module.css"
import Input from "antd/lib/input";
import {Ingredient} from "./components/ingredient";
import {Cost} from "./components/ingredient/cost";

class App extends React.Component {

    state = {cereals: 0}

    onInputChange = ({target: {value}}) => {
        const qty = parseInt(value)
        this.setState({cereals: isNaN(qty) ? 0 : qty})
    }

    render() {
        const {cereals} = this.state
        const beerTotal = Math.floor(cereals / 5)
        const ingredients = {
            mineralWater: beerTotal * 6,
            sugar: beerTotal,
            leaveningAgent: beerTotal * 2
        }
        const costs = {
            mineralWater: ingredients.mineralWater * 30,
            sugar: ingredients.sugar * 20,
            leaveningAgent: ingredients.leaveningAgent * 20
        }
        const totalCost = Object.values(costs).reduce((a, b) => a + b, 0)
        return (
            <div className={styles.container}>
                <table>
                    <tbody>
                    <tr>
                        <td>
                            <img src="images/item_cereals.png" alt="Cereals"/>
                        </td>
                        <td className={styles.ingredientLabel}>Cereals</td>
                        <td colSpan={2}>
                            <Input onChange={this.onInputChange} value={Number(cereals).toString()} defaultValue={0}
                                   step={1}
                                   min={0}
                                   type="number"/>
                        </td>
                    </tr>
                    <Ingredient
                        cost={`${costs.mineralWater}(30)`}
                        icon="item_water"
                        label="Mineral Water"
                        value={ingredients.mineralWater}
                    />
                    <Ingredient
                        cost={`${costs.sugar}(20)`}
                        icon="item_sugar"
                        label="Sugar"
                        value={ingredients.sugar}
                    />
                    <Ingredient
                        cost={`${costs.leaveningAgent}(20)`}
                        icon="item_leavening_agent"
                        label="Leavening Agent"
                        value={ingredients.leaveningAgent}
                    />
                    </tbody>
                    <tfoot>
                    <tr>
                        <td colSpan={3}/>
                        <td><Cost cost={totalCost}/> </td>
                    </tr>
                    </tfoot>
                </table>
            </div>
        )

    }
}

export default App;
