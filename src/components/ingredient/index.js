import React from "react";
import {Cost} from "./cost";

export const Ingredient = ({label, value, icon, cost}) => (
    <tr>
        <td><img src={`images/${icon}.png`} alt={label}/></td>
        <td><p>{label}</p></td>
        <td><p>{value}</p></td>
        <td><Cost cost={cost}/></td>
    </tr>
)
