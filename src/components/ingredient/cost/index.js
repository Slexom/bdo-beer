import React from "react"
import styles from "./style.module.css"


export const Cost = ({cost}) => (
    <div className={styles.container}>
        <img src={`images/item_silver.png`} alt="Silver" className={styles.silver}/>
        <p>{cost}</p>
    </div>
)
